#Makefile

obj_name = Emmanuel_Goldstein_Dissertation
obj_name2 = $(obj_name)_handout
pdfs = $(obj_name).pdf $(obj_name2).pdf

all: $(pdfs)

$(obj_name).pdf: $(obj_name).tex
	pdflatex -synctex=1 $<
	bibtex $(obj_name)
	makeglossaries $(obj_name)
	pdflatex -synctex=1 $<
	pdflatex -synctex=1 $<	
	rm -f *.acn
	rm -f *.acr
	rm -f *.alg
	rm -f *.aux
	rm -f *.bbl
	rm -f *.blg
	rm -f *.glg
	rm -f *.glo
	rm -f *.gls
	rm -f *.ist
	rm -f *.lof
	rm -f *.log
	rm -f *.lot
	rm -f *.nlo
	rm -f *.out
	rm -f *.synctex.gz
	rm -f *.toc

$(obj_name2).pdf: $(obj_name2).tex
	pdflatex -synctex=1 $<
	makeglossaries $(obj_name2)
	pdflatex -synctex=1 $<	
	rm -f *.acn
	rm -f *.acr
	rm -f *.alg
	rm -f *.aux
	rm -f *.bbl
	rm -f *.blg
	rm -f *.glg
	rm -f *.glo
	rm -f *.gls
	rm -f *.ist
	rm -f *.lof
	rm -f *.log
	rm -f *.lot
	rm -f *.nlo
	rm -f *.out
	rm -f *.synctex.gz
	rm -f *.toc


.PHONY: install
