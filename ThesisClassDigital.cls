% Thesis class definition for Vanderbilt University's Graduate School guidelines.
% Written by Will Hedgecock, Fall 2013
% Modified by Ben Babjak, Fall 2014

% Declare package
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ThesisClassDigital}[2014/04/10 Vanderbilt University's Thesis Class]

% Options
\newif\if@DigitalOutput\@DigitalOutputfalse
\DeclareOption{Digital}{\@DigitalOutputtrue}
%\DigitalOptionSelectedtrue}

% Pass default options
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}
\ProcessOptions\relax
\LoadClass{report}

% Include necessary packages 
\RequirePackage[letterpaper,left=1in,top=1in,right=1in,bottom=1in,nohead,footskip=0.5in]{geometry}
\RequirePackage{indentfirst}
\RequirePackage{float}
\RequirePackage{graphicx}
\RequirePackage{setspace}
\RequirePackage{listings}
\RequirePackage{chngcntr}
\RequirePackage[cmex10]{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{url}
%\RequirePackage{subfig}
\RequirePackage[format=hang,labelfont=bf,font=small]{caption}
\RequirePackage{ifthen}
%\RequirePackage{array,multirow}
%\RequirePackage{algorithm}
%\RequirePackage{algpseudocode}
\RequirePackage{nomencl}
\RequirePackage{hyperref}
\RequirePackage[numbered]{bookmark}
%\RequirePackage[backend=biber]{biblatex}
\hypersetup{pdfborder = {0 0 0}}


\makenomenclature
\errorcontextlines 999

% Set onehalfspacing and place captions in correct place
\floatstyle{plain}
\restylefloat{figure}
\newcommand{\doublespacesize}{1.66}
\newcommand{\singlespacesize}{1}
\renewcommand{\baselinestretch}{1}

% Print chapter number in roman numerals and set numbers
\renewcommand{\thechapter}{\texorpdfstring{\Roman{chapter}}{Chap.~\Roman{chapter}:}}
\setcounter{secnumdepth}{0}
\setcounter{tocdepth}{2}
\counterwithout{table}{chapter}
\counterwithout{figure}{chapter}
\counterwithout{equation}{chapter}
\newcommand{\secref}[1]{``Section: \textit{\nameref{#1}}'' in Chapter \ref{#1}}

% Set the names of these areas to be what we want, all caps
\if@DigitalOutput 
\renewcommand{\contentsname}{TABLE OF CONTENTS}
\renewcommand{\listfigurename}{LIST OF FIGURES}
\renewcommand{\listtablename}{LIST OF TABLES}
\renewcommand{\nomname}{LIST OF ABBREVIATIONS}
\renewcommand{\bibname}{REFERENCES}
\renewcommand{\chaptername}{CHAPTER}
\renewcommand{\appendixname}{APPENDIX}
\fi

% Setup margins
\pdfpagewidth 8.5in
\pdfpageheight 11in
\widowpenalty=10000
\clubpenalty=10000
\brokenpenalty=10000

% Allow for running fractions
\newcommand*\rfrac[2]{{}^{#1}\!/_{#2}}

% Allow for thick hlines
\def\hlinewd#1{%
\noalign{\ifnum0=`}\fi\hrule \@height #1 %
\futurelet\reserved@a\@xhline}

% Allow for multiple parts of an algorithm
\newcommand\Algphase[1]{%
\itemsep 0.1em
\vspace*{-.7\baselineskip}\Statex\hspace*{\dimexpr-\algorithmicindent-3pt\relax}\rule{\textwidth}{0.4pt}%
\Statex\vspace{-3pt}\hspace*{\dimexpr-\algorithmicindent-3pt\relax}\textit{\textbf{#1}}%
\vspace*{-.7\baselineskip}\Statex\hspace*{\dimexpr-\algorithmicindent-3pt\relax}\rule{\textwidth}{0.4pt}%
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Redefine the macro used for floats so that single spacing is used
%\def\@xfloat#1[#2]{
%    \ifhmode \@bsphack\@floatpenalty -\@Mii
%    \else \@floatpenalty-\@Miii\fi
%    \def\@captype{#1}
%    \ifinner \@parmoderr\@floatpenalty\z@
%    \else \@next\@currbox\@freelist{\@tempcnta\csname ftype@#1\endcsname
%        \multiply\@tempcnta\@xxxii\advance\@tempcnta\sixt@@n
%        \@tfor \@tempa :=#2\do
%        {
%            \if\@tempa h\advance\@tempcnta \@ne\fi
%            \if\@tempa t\advance\@tempcnta \tw@\fi
%            \if\@tempa b\advance\@tempcnta 4\relax\fi
%            \if\@tempa p\advance\@tempcnta 8\relax\fi
%        }
%        \global\count\@currbox\@tempcnta}\@fltovf
%    \fi
%    \global\setbox\@currbox\vbox\bgroup 
%    \def\baselinestretch{1}\@normalsize
%    \boxmaxdepth\z@
%    \hsize\columnwidth \@parboxrestore
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Redefine the macro used for footnotes to use single spacing
\long\def\@footnotetext#1{
    \insert\footins
	{
	    \def\baselinestretch{1}\footnotesize
		\interlinepenalty\interfootnotelinepenalty 
		\splittopskip\footnotesep
		\splitmaxdepth \dp\strutbox \floatingpenalty \@MM
		\hsize\columnwidth \@parboxrestore
		\edef\@currentlabel{\csname p@footnote\endcsname\@thefnmark}\@makefntext
		{\rule{\z@}{\footnotesep}\ignorespaces
		#1\strut}
	}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define document-wide information macros
\def\department#1{\gdef\@department{#1}}
\def\advisor#1{\gdef\@advisor{#1}}
\def\firstreader#1{\gdef\@firstreader{#1}}
\def\secondreader#1{\gdef\@secondreader{#1}}
\def\thirdreader#1{\gdef\@thirdreader{#1}}
\def\fourthreader#1{\gdef\@fourthreader{#1}}
\def\fifthreader#1{\gdef\@fifthreader{#1}}
\def\submitdate#1{\gdef\@submitdate{#1}}
\def\copyrightyear#1{\gdef\@copyrightyear{#1}}
\def\@title{}\def\@author{}\def\@department{}
\def\@advisor{}
\def\@firstreader{}\def\@secondreader{}\def\@thirdreader{}
\def\@fourthreader{}\def\@fifthreader{}
\def\@submitdate{\ifcase\the\month\or
  January\or February\or March\or April\or May\or June\or
  July\or August\or September\or October\or November\or December\fi
  ,\space \number\the\year}
\ifnum\month=12
    \@tempcnta=\year \advance\@tempcnta by 1
    \edef\@copyrightyear{\number\the\@tempcnta}
\else
    \def\@copyrightyear{\number\the\year}
\fi\vskip 0.5in
\newif\ifcopyright
\copyrighttrue \vskip 0.5in
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define title page format and creation macro
\def\titlepage{%
  \thispagestyle{empty}%
  \begin{center}
  \doublespacing
  {
    \large\expandafter{\@title}
  }\\ 
  \vskip 0.1in \vfil By\\
  \vskip 0.1in \vfil \@author\\ 
  \vskip 0.1in \vfil Dissertation\\
  Submitted to the Faculty of the\\
  Graduate School of Vanderbilt University\\
  in partial fulfillment of the requirements\\
  for the degree of\\ 
  \vskip 0.1in \vfil DOCTOR OF PHILOSOPHY\\
  in\\ 
  \vskip 0.1in \vfil \@department\\ 
  \vskip 0.1in \vfil \@submitdate\\ 
  \vskip 0.1in \vfil Nashville, Tennessee\\ 
  \if@DigitalOutput
    \vskip 0.8in \vfil 
    Approved:\\ 
    \@firstreader\\ 
    \@secondreader\\ 
    \@thirdreader\\ 
    \@fourthreader\\ 
    \@fifthreader
    \end{center}
  \else
    \end{center}
    \vskip 0.5in \vfil 
    \parbox{11cm}{Approved:} \hfill \parbox{4cm}{Date:} \\
    \vspace{-0.1cm}\\
    \rule{11cm}{0.2mm} \hfill \rule{4cm}{0.2mm}\\
    \vspace{-0.65cm}\\
    \@firstreader\\
    \vspace{-0.3cm}\\
    \rule{11cm}{0.2mm} \hfill \rule{4cm}{0.2mm}\\
    \vspace{-0.65cm}\\
    \@secondreader\\ 
    \vspace{-0.3cm}\\
    \rule{11cm}{0.2mm} \hfill \rule{4cm}{0.2mm}\\
    \vspace{-0.65cm}\\
    \@thirdreader\\ 
    \vspace{-0.3cm}\\
    \rule{11cm}{0.2mm} \hfill \rule{4cm}{0.2mm}\\
    \vspace{-0.65cm}\\
    \@fourthreader\\ 
    \vspace{-0.3cm}\\
    \rule{11cm}{0.2mm} \hfill \rule{4cm}{0.2mm}\\
    \vspace{-0.65cm}\\
    \@fifthreader
  \fi
  
  \newpage
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define copyright page format
\def\copyrightpage{%
	\null\nointerlineskip
	\vfill
	\begin{center}
	    Copyright \copyright\ \@copyrightyear\ by \@author \\
		All Rights Reserved
	\end{center}
	\vfill\newpage}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define preliminary pages 
\def\maketitlepages{
  \pagenumbering{roman}
	\pagestyle{plain}
	\titlepage
  \if@DigitalOutput
    \ifcopyright
      \copyrightpage
    \fi
    \addtocontents{toc}{\hfill Page \vskip 0.01em}
  \fi
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Include a preface section before the table of contents
\newcommand{\prefacechapter}[1]
{
  \chapter*{#1}
  \addcontentsline{toc}{chapter}{\protect\texorpdfstring{\uppercase{#1}}{#1}}
	\onehalfspacing
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Finish making all prelimary pages including table of contents and lists
\def\afterpreface{
	\begin{spacing}{\singlespacesize}
	\newpage \tableofcontents \cleardoublepage\phantomsection
	\addcontentsline{toc}{chapter}{\listtablename} \addtocontents{lot}{\protect\flushleft Table \hfill Page} \addtocontents{lot}{} \listoftables \cleardoublepage\phantomsection
	\addcontentsline{toc}{chapter}{\listfigurename} \addtocontents{lof}{\protect\flushleft Figure \hfill Page} \addtocontents{lof}{} \listoffigures \cleardoublepage\phantomsection
	\addcontentsline{toc}{chapter}{\nomname} \printglossary[title=\nomname,type=\acronymtype] % prints just the list of acronyms 
	\newpage
	\addtocontents{toc}{\protect\flushleft \vskip -0.1em Chapter} \pagenumbering{arabic} \pagestyle{plain}
	\end{spacing}
	\setcounter{chapter}{0}
	\onehalfspacing
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Redefine \thebibliography to go to a new page and put an entry in the table of contents
\let\@ldthebibliography\thebibliography
\renewcommand{\thebibliography}[1]{\cleardoublepage\phantomsection \addcontentsline{toc}{chapter}{\bibname} \singlespacing
    \@ldthebibliography{#1}}

%\let\@ldprintbibliography\printbibliography
%\renewcommand{\printbibliography}[0]{\cleardoublepage\phantomsection \addcontentsline{toc}{chapter}{REFERENCES\bibname} \singlespacing
%    \@ldprintbibliography}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Abstract environment definition
\if@DigitalOutput
\renewenvironment{abstract}{
  \chapter*{Preface}
  \bgroup
  \pagestyle{empty}
  {
    \centering
  } 
  \par\doublespacing
}
{  
   {\vfill\egroup}
}
\else%
\renewenvironment{abstract}{
  \bgroup
  \newpage
  \newcounter{savepageno}
  \setcounter{savepageno}{\value{page}}
  \thispagestyle{empty}
  \pagestyle{empty}
  \hfill\normalsize\rm\underline{\MakeUppercase{\@department}}\par
  {
    \centering
    \vspace{1.0\baselineskip}
    \normalsize\expandafter{\@title}\\
    \vspace{1.0\baselineskip}
    \expandafter{\@author}\\
    \vspace{1.0\baselineskip}
    \underline{Dissertation under the direction of Professor \@advisor}\\
  }
  \par\doublespacing
}
{ 
   {\vfill \vfill
   \hskip -0.27in Approved\,\rule[-1mm]{3.4in}{0.4pt} \hfill \ Date\,\rule[-1mm]{1.5in}{0.4pt}\\[-8pt]
   \hskip -0.27in \phantom{Approved\,\,\,}\small\expandafter{\@advisor}
   \par\newpage\setcounter{page}{\value{savepageno}}\egroup}
}
\fi%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Single-space quote and quotation environments
\renewenvironment{quotation}
    {\list{}{\listparindent 1.5em%
	\itemindent    \listparindent
	\rightmargin   \leftmargin
	\parsep        \z@ \@plus\p@}%
	\begin{spacing}{\singlespacesize} \item\relax}
	{\end{spacing}\endlist}
\renewenvironment{quote}
    {\list{}{\rightmargin\leftmargin}%
	\begin{spacing}{\singlespacesize} \item\relax}
	{\end{spacing}\endlist}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Setup correct Chapter heading formats
\renewcommand\@makechapterhead[1]{%
    {\parindent \z@ \centering \normalfont
     \ifnum \c@secnumdepth >\m@ne
	    \ifthenelse{\equal{\chaptername}{\appendixname}}
			{\hypertarget{\chaptername.\thechapter}}{\hypertarget{\chaptername.\arabic{chapter}}}
        \bfseries \uppercase{\chaptername}\space\thechapter\vspace{6ex}
        \par\nobreak
     \fi
     \interlinepenalty\@M
     \bfseries \uppercase{#1}\par\nobreak \vspace{6ex}
    }
}

\renewcommand\@makeschapterhead[1]{%
\begin{spacing}{1}
    {\parindent \z@ \centering \normalfont
     \interlinepenalty\@M
	 \ifthenelse{\value{chapter}>0}{\hypertarget{\chaptername*.\arabic{chapter}}}{}
     \bfseries \uppercase{#1}\par\nobreak \vspace{4ex}
    }
\end{spacing}
}

\renewcommand\section{\@startsection{section}{1}{\z@}%
  {2em}{0.1em}%
  {\centering \normalfont \normalsize \bf%
  \addtocounter{section}{1}
  \ifnum\value{section}>1 \else \addtocontents{toc}{\protect\addvspace{1em}}\fi}}
\renewcommand\subsection{\vskip 2em \@startsection{subsection}{2}{\z@}%
  {0em}{0.1em}%
  {\raggedright \normalfont \normalsize \bf}}
\renewcommand\subsubsection{\vskip 2em \@startsection{subsubsection}{3}{\z@}%
  {0.1em}{0.1em}%
  {\raggedright \normalfont \normalsize \bf \itshape}}
\renewcommand\paragraph{\vskip 2em \@startsection{paragraph}{4}{\z@}%
  {0.1em}{0.1em}%
  {\raggedright \normalfont \normalsize \itshape}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fix Table of Contents format
\def\l@chapter#1#2{\pagebreak[3]
    \vskip 1em plus 1pt
	\@dottedtocline{0}{0em}{2.7em}{#1}{#2}
}

% Print sub-chapter lines indented appropriately
\renewcommand{\@dotsep}{2}
\renewcommand\l@section{\@dottedtocline{1}{2.7em}{0em}}
\renewcommand\l@subsection{\@dottedtocline{2}{4.2em}{0em}}
\renewcommand\l@subsubsection{\@dottedtocline{3}{5.7em}{0em}}
\renewcommand\l@paragraph{\@dottedtocline{4}{7.2em}{0em}}
\renewcommand\l@subparagraph{\@dottedtocline{5}{8.7em}{0em}}

% Put a blank line above figure and table entries in their lists
\renewcommand{\l@figure}{
	\protect\flushleft
	\@dottedtocline{0}{0em}{2.3em}}
\renewcommand{\l@table}{\l@figure}

% Put a period after the item number in the table of contents
\def\numberline#1{\hb@xt@\@tempdima{{#1.}\hfil}}

% Redefine \appendix to put an entry in the table of contents
\let\@ldappendix\appendix
\renewcommand{\appendix}{
    \@ldappendix
	\addtocontents{toc}{\protect\flushleft \vskip -0.1em Appendix}
	\renewcommand{\chaptername}{APPENDIX}
	\renewcommand{\thechapter}{\texorpdfstring{\Alph{chapter}}{App.~\Alph{chapter}:}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
